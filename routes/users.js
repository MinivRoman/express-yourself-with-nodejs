const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const userRepository = require('../repositories/user.repository');

const jsonParser = bodyParser.json();

/* GET */

router.get('/', (req, res, next) => {
  const userList = userRepository.getUserList();

  if (userList) {
    res.send(userList);
  } else {
    res.sendStatus(500);
  }
});

router.get('/:id', (req, res, next) => {
  const id = req.params['id'];
  const user = userRepository.getUser(id);

  if (user) {
    res.send(user);
  } else {
    res.status(404).send('User not found.');
  }
});

/* POST */

router.post('/', jsonParser, (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.sendStatus(400);
  }

  const user = req.body;
  const addedUser = userRepository.addUser(user);

  if (addedUser) {
    res.send(addedUser);
  } else {
    res.status(400).send('Invalid data.')
  }
});

/* PUT */

router.put('/:id', jsonParser, (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.sendStatus(400);
  }
  
  const id = req.params['id'];
  const user = req.body;
  user._id = id;
  const updatedUser = userRepository.updateUser(user);

  if (updatedUser) {
    res.send(updatedUser);
  } else {
    res.status(404).send('User not found.');
  }
});

/* DELETE */

router.delete('/:id', (req, res, next) => {
  const id = req.params['id'];
  const user = userRepository.deleteUser(id);

  if (user) {
    res.send(user);
  } else {
    res.status(404).send('User not found.');
  }
});

module.exports = router;
