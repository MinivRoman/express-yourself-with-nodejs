const request = require('supertest');

const app = require('../app');

describe('Tests index routes', () => {
    it("should return: 'GET home page.'", (done) => {
        request(app)
            .get('/')
            .expect('GET home page.')
            .end(done);
    });
});
