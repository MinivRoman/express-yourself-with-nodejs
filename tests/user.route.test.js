const request = require('supertest');
const assert = require('assert');

const app = require('../app');
const userRepository = require('../repositories/user.repository');

describe('Tests user routes', () => {
    describe('GET: /user', () => {
        it('should return: user array', (done) => {
            request(app)
                .get('/user')
                .expect((res) => {
                    assert.deepEqual(res.body, userRepository.getUserList());
                })
                .end(done);
        });
    });

    describe('GET: /user/:id', () => {
        it('should return: user object', (done) => {
            request(app)
                .get('/user/10')
                .expect((res) => {
                    assert.deepEqual(res.body, userRepository.getUser('10'));
                })
                .end(done);
        });

        it('should return: user not found', (done) => {
            request(app)
                .get('/user/someUser')
                .expect(404)
                .expect('User not found.')
                .end(done);
        });
    });

    describe('POST: /user', () => {
        it('should return: bad request (empty body)', (done) => {
            request(app)
                .post('/user')
                .expect(400)
                .end(done);
        });
    });

    describe('PUT: /user/:id', () => {
        it('should return: bad request (empty body)', (done) => {
            request(app)
                .put('/user/someUser')
                .expect(400)
                .end(done);
        });
    });
    
    describe('DELETE: /user/:id', () => {
        it('should return: user not found', (done) => {
            request(app)
                .delete('/user/someUser')
                .expect(404)
                .expect('User not found.')
                .end(done);
        });
    });
});
